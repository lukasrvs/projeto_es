<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>


<body class="d-flex flex-column" style="color: black;">
    <div class="container">
      <div class="container text-center">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <h1  style="font-size: 70px;font-weight: bolder;">Sequencial Numeral</h1>
          </div>
        </div>
        <div class="generator">
          <div class="panel-body" ondragover="overAction(event)" ondrop="dropAction(event)">
              <p style="font-size: 80px">
              <?php
                $a=array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20");
                $random_keys=array_rand($a,10);
                echo $a[$random_keys[0]]."   ";
                echo $a[$random_keys[1]]."   ";
                echo $a[$random_keys[2]]."   ";
                echo $a[$random_keys[3]]."   ";
                echo $a[$random_keys[4]]."   ";
                echo $a[$random_keys[5]]."   ";
                echo $a[$random_keys[6]]."   ";
                echo $a[$random_keys[7]]."   ";
                echo $a[$random_keys[8]]."   ";
                echo $a[$random_keys[9]];
                ?></p>
          </div>
        </div>
        <form>
  <div class="form-row">
    
    <div class="row">
        <div class="form-group col-md-2">
        <input type="text" class="form-control" id="vet1">
      </div>

      <div class="form-group col-md-2">
        <input type="text" class="form-control" id="vet2" >
      </div>

      <div class="form-group col-md-2">
        <input type="text" class="form-control" id="vet3">
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" id="vet4">
      </div>

      <div class="form-group col-md-2">
        <input type="text" class="form-control" id="vet5">
      </div>

    </div>
    
    <div class="row">
      
      <div class="form-group col-md-2">
      <input type="text" class="form-control" id="vet6">
    </div>

    <div class="form-group col-md-2">
      <input type="text" class="form-control" id="vet7">
    </div>

    <div class="form-group col-md-2">
      <input type="text" class="form-control" id="vet8">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" id="vet9">
    </div>

    <div class="form-group col-md-2">
      <input type="text" class="form-control" id="vet10">
    </div>

    </div>


  </div>
  
</form>
      </div>
    </div>
    
    <footer id="sticky-footer" class="py-4 bg-secondary text-white-50">
      <div class="container text-center">
        <small>Copyright &copy; github.com/lukasrvs</small>
      </div>
    </footer>
  </body>

  <script>
  
  </script>
</html>